module.exports = () => ({
    module: {
    rules: [
        {
            test: /\.(css|scss|sass)$/,
            use: ['style-loader', 'css-loader', 'fast-sass-loader']
        }
    ]
   }
})
